variable "civo_token" {}
variable "cloudflare_token" {}

variable "name" {
  default = "david"
}

terraform {
  required_providers {
    civo = {
      source = "civo/civo"
    }

    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}

provider "civo" {
  token  = var.civo_token
  region = "LON1"
}


provider "cloudflare" {
  api_token = var.cloudflare_token
}

data "cloudflare_zone" "rawkode_academy" {
  name       = "rawkode.academy"
  account_id = "0aeb879de8e3cdde5fb3d413025222ce"
}

data "civo_reserved_ip" "platform" {
  name = "platform"
}

resource "cloudflare_record" "example" {
  zone_id = data.cloudflare_zone.rawkode_academy.id
  name    = "env0"
  value   = data.civo_reserved_ip.platform.ip
  type    = "A"
  ttl     = 3600
  comment = "Hello Env0"
}
