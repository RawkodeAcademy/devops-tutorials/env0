import * as civo from "@pulumi/civo";

const firewall = new civo.Firewall("platform", {
	name: "platform",
	region: "LON1",
	createDefaultRules: true,
});

const cluster = new civo.KubernetesCluster("platform", {
	name: "platform",
	pools: {
		nodeCount: 3,
		size: "g4s.kube.medium",
	},
	region: "LON1",
	firewallId: firewall.id,
});

const loadBalancerIp = new civo.ReservedIp("platform", {
	name: "platform",
});
const loadBalancerAppIp = new civo.ReservedIp("platform-app", {
	name: "app",
});

export const loadBalancerIpId = loadBalancerIp.ip;
export const kubeconfig = cluster.kubeconfig;
